<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::get('/surah/{surah}', 'SurahController@show')->name('baca-surah');
Route::get('/surah/{surah}/{ayah}', 'SurahController@showAyah')->name('baca-ayah');
Route::get('/tafsir/{surah}/{ayah}', 'TafsirController@show')->name('baca-tafsir');

Route::get('/cari/{kata}', 'UserProfileController@show')->name('cari');
Route::get('/hal/{slug}', 'ContentController@getbyslug')->name('hal');
Route::get('/tentang', 'ContentController@tentang')->name('tentang');


Route::prefix('tools')->group(function () {
    Route::get('getmeta', 'ToolsController@getmeta')->name('tools-getmeta');
    Route::get('addslug', 'ToolsController@addslug')->name('tools-addslug');
    Route::get('addsound','ToolsController@addsound')->name('tools-addsound');
    Route::get('generateindex','ToolsController@generateindex')->name('tools-generateindex');

});