<?php
if (! function_exists('getAyatNumberbyAyatID')) {
    function getVerseIDbyAyatID($AyahID) {
        $ayahdetail = \DB::table('quran_ar')->select('VerseID')->where('ID','=',$AyahID)->get();
        return $ayahdetail[0]->VerseID;
    }
}