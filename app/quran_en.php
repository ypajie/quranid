<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quran_en extends Model
{
    //
     protected $table = 'quran_en';
     protected $primaryKey = 'ID';
     public $timestamps = false;
}
