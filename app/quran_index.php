<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quran_index extends Model
{
    //
    protected $table = 'quran_index';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
