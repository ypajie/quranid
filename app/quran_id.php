<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quran_id extends Model
{
    //
    protected $table = 'quran_id';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
