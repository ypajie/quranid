<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use AlgoliaSearch;

class Send2Algolia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algolia:copyindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy table quran_index to algolia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $client = new \AlgoliaSearch\Client('IM3G5LPHPY', '49f4ed7b388d9770506efb7b4e8ff3d2');
        $index = $client->initIndex('quranku-v1');


        $ayahs = \App\quran_index::orderBy('ID', 'asc')->get();

        $counter = 0;
        $catalog = array();
        foreach($ayahs as $a){
                
             $catalog[$counter]['objectID']     = $a->ID;
             $catalog[$counter]['surah_index']  = $a->surah_index;
             $catalog[$counter]['ayah_index']   = $a->ayah_index;
             $catalog[$counter]['arabic']       = $a->arabic;
             $catalog[$counter]['bahasa']       = $a->bahasa;
             $catalog[$counter]['english']      = $a->english;
             $catalog[$counter]['surah_name']   = $a->surah_name;
             $catalog[$counter]['surah_tname']  = $a->surah_tname;
             $catalog[$counter]['surah_slug']  = $a->surah_slug;
             $catalog[$counter]['surah_type']   = $a->surah_type;
             $counter++;
        }


        $chunks = array_chunk($catalog, 1000);

            foreach ($chunks as $batch) {

              $b = (object) $batch;
              $x = $index->saveObjects($b);
              $index->waitTask($x['taskID']);
            }

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
