<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quran_surah extends Model
{
    //
    protected $table = 'quran_surah';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
