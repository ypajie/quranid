<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quran_transliteration extends Model
{
    
    protected $table = 'quran_transliteration';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
