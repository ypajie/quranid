<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class AyahController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($surah,$ayah){
        echo $surah . "-" .$ayah;

        exit;
        //get all surah
        $surahs = \App\quran_surah::where('slug','<>',$surah)->get();
        $viewdata['surahs'] =  $surahs;


        //get current surah meta 
        $surah_meta = \App\quran_surah::where('slug','=',$surah)->get();
        $surahIndex = $surah_meta[0]->index;
        $viewdata['surah_meta'] =  $surah_meta[0];
      
         //get current surah content
        $surah = \DB::table('quran_ar')
            ->join('quran_id', 'quran_ar.verseID', '=', 'quran_id.verseID')
            ->select('quran_id.SuraID as SuraID','quran_id.VerseID as VerseID','quran_id.ayahText as translation', 'quran_ar.ayahText as arabic')
            ->where('quran_ar.suraID','=',$surahIndex)
            ->where('quran_id.suraID','=',$surahIndex)
            ->orderBy('quran_id.VerseID','asc')
            ->get();
      
       
        $viewdata['surah'] =  $surah;
        
        return \View::make('ayah')->with('viewdata', $viewdata);
    }
}