<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class HomeController extends Controller{
    
    public function index(){
        $surahs = \App\quran_surah::all();
        $viewdata['surahs'] =  $surahs;
        //return view('index',$viewdata);
        return \View::make('index')->with('viewdata', $viewdata);
    }
}