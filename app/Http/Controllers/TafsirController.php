<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class TafsirController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($surah,$ayah=null){

        //get all surah
        $surahs = \App\quran_surah::where('slug','<>',$surah)->get();
        $viewdata['surahs'] =  $surahs;

        //get current surah meta 
        $surah_meta = \App\quran_surah::where('slug','=',$surah)->get();
        $surahIndex = $surah_meta[0]->index;
        $viewdata['surah_meta'] =  $surah_meta[0];
      
         //get current surah content
        $ayahdetail = \DB::table('quran_ar')
            ->join('quran_id', 'quran_ar.verseID', '=', 'quran_id.verseID')
            ->select('quran_id.SuraID as SuraID','quran_id.id as id','quran_id.VerseID as VerseID','quran_id.ayahText as translation', 'quran_ar.ayahText as arabic')
            ->where('quran_id.VerseID','=',$ayah)
            ->where('quran_ar.suraID','=',$surahIndex)
            ->where('quran_id.suraID','=',$surahIndex)
            ->orderBy('quran_id.VerseID','asc')
            ->get();

        $tafsirdetail = \DB::table('content_ayah')
            ->join('quran_id', 'content_ayah.ayah_id', '=', 'quran_id.verseID')
            ->join('contents','content_ayah.content_id','=','contents.c_id')
            ->select('contents.c_ayat as ayahs','contents.c_asbab as asbab','contents.c_tafsir as tafsir','contents.c_fiqih as fiqih')
            ->where('contents.deleted_at',null)
            ->where('content_ayah.ayah_id',$ayahdetail[0]->id)
            ->first();
        if(isset($tafsirdetail->ayahs)){
            $ayahs = json_decode($tafsirdetail->ayahs);
            if(count($ayahs) > 1){
                $start_range    = getVerseIDbyAyatID($ayahs[0]);
                $end_range      = getVerseIDbyAyatID($ayahs[count($ayahs)-1]);
                $ayah_range     = $start_range ." - ".$end_range;
            }else{
                $ayah_range = $start_range;
            }
        }else{
            $ayah_range = '';
        }
       
        $viewdata['surah']          =  $surah;
        $viewdata['selectedAyah']   =  $ayah;
        $viewdata['ayahdetail']     =  $ayahdetail[0];
        if(isset($tafsirdetail->ayahs)){
            $viewdata['tafsirdetail']   =  $tafsirdetail;
        }else{
            $viewdata['tafsirdetail'] ='';
        }
        $viewdata['ayah_range']     =  $ayah_range;
        
        return \View::make('tafsir')->with('viewdata', $viewdata);
    }


    public function showAyah($surah,$ayah=null){

        //get all surah
        $surahs = \App\quran_surah::where('slug','<>',$surah)->get();
        $viewdata['surahs'] =  $surahs;

        //get current surah meta 
        $surah_meta = \App\quran_surah::where('slug','=',$surah)->get();
        $surahIndex = $surah_meta[0]->index;
        $viewdata['surah_meta'] =  $surah_meta[0];
      
         //get current surah content
        $surah = \DB::table('quran_ar')
            ->join('quran_id', 'quran_ar.verseID', '=', 'quran_id.verseID')
            ->select('quran_id.SuraID as SuraID','quran_id.VerseID as VerseID','quran_id.ayahText as translation', 'quran_ar.ayahText as arabic')
            ->where('quran_ar.suraID','=',$surahIndex)
            ->where('quran_id.suraID','=',$surahIndex)
            ->orderBy('quran_id.VerseID','asc')
            ->get();
      
       
       

        $viewdata['surah'] =  $surah;
        $viewdata['selectedAyah'] =  $ayah;
        
        
        return \View::make('surah')->with('viewdata', $viewdata);
    }


    public function showByIndex($surah_index,$ayah_index){
        echo $surah_index."-- <br> --".$ayah_index;
    }
}