<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class ToolsController extends Controller{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function getmeta(){
		    
		   $xp = "/var/www/html/quranid/public/assets/quran-metadata.xml";
		   $xml=simplexml_load_file($xp) or die("Error: Cannot create object");
		   $suras = $xml->suras[0]->sura;
		   foreach($suras as $s){

		   	echo "insert into quran_surah(`index`,`ayahs`,`start`,`name`,`tname`,`ename`,`type`,`order`,`rukus`) values('".$s['index']."','".$s['ayas']."','".$s['start']."','".$s['name']."','".$s['tname']."','".$s['ename']."','".$s['type']."','".$s['order']."','".$s['rukus']."');<br>";
		   
		   }
		  
				
    }


    public function updatesurahnames(){
        $surahs = \App\quran_surah::all();
     
       foreach($surahs as $surah){
              $slug = trim($surah->tname);
              $slug = trim(strtolower(preg_replace("/ /","-",$slug)));
             
               echo "update quran_index set surah_tname=\"".$surah->tname."\" where ID=\"".$surah->ID."\";<br>";
       }

  }

    public function addslug(){
		  $surahs = \App\quran_surah::all();
       
         foreach($surahs as $surah){
                $slug = trim($surah->tname);
                $slug = trim(strtolower(preg_replace("/ /","-",$slug)));
               // $slug = preg_replace("/ /","-",$slug);
         		echo "update quran_surah set slug=\"".$slug."\" where ID=\"".$surah->ID."\";<br>";
         }

    }


    public function addsound(){
    	$ayahs = \App\quran_ar::all();

    	foreach($ayahs as $a){
    			
    		echo str_pad($input, 10, "_", STR_PAD_BOTH);
    	}

    	
    }

    public function generateindex(){
       

         $ayahs = \DB::table('quran_ar')
            ->join('quran_id', 'quran_ar.ID', '=', 'quran_id.ID')
            ->join('quran_en','quran_ar.ID','=','quran_en.ID')
            ->join('quran_transliteration','quran_ar.ID','=','quran_transliteration.ID')
            ->join('quran_surah','quran_ar.suraID','=','quran_surah.ID')
            ->select('quran_surah.ID as surah_index', 
                     'quran_ar.VerseID as ayah_index',
                     'quran_ar.AyahText as arabic',
                     'quran_id.AyahText as bahasa',
                     'quran_en.AyahText as english',
                     'quran_transliteration.AyahText as transliteration',
                     'quran_surah.name as surah_name',
                     'quran_surah.tname as surah_tname',
                     'quran_surah.type as surah_type'
                     

                        )
            ->orderBy('quran_ar.ID','asc')
            ->get();
        $counter =1;
        foreach($ayahs as $a){
                $slug = trim($a->surah_tname);
                $slug = trim(strtolower(preg_replace("/ /","-",$slug)));
            echo 'INSERT INTO `quranku`.`quran_index`
                        (`ID`,`surah_index`,
                        `ayah_index`,
                        `arabic`,
                        `bahasa`,
                        `english`,
                        `transliteration`,
                        `surah_name`,
                        `surah_tname`,
                        `surah_slug`,
                        `surah_type`,
                        `created_at`,
                        `indexed_at`,
                        `updated_at`)
                        VALUES
                        ("'.$counter.'","'.$a->surah_index.'",
                        "'.$a->ayah_index.'",
                        "'.$a->arabic.'",
                        "'.addslashes($a->bahasa).'",
                        "'.addslashes($a->english).'",
                        "'.addslashes($a->transliteration).'",
                        "'.$a->surah_name.'",
                        "'.addslashes($a->surah_tname).'",
                        "'.$slug.'",
                        "'.$a->surah_type.'",
                        "'.date("Y-m-d H:i:s").'",
                        NULL,
                        "'.date("Y-m-d H:i:s").'");<br>';
            $counter++;

        }

        
    }
}










