

@extends('layouts.app')
@section('title', "Surah")


@section('opengraph')

<?php 

if(isset($viewdata['selectedAyah'])){ ?>
<?php
    $audiofiles = '';
    $ogaudiofile = str_pad($viewdata['surah_meta']->ID,3,"0",STR_PAD_LEFT).str_pad($viewdata['selectedAyah'],3,"0",STR_PAD_LEFT);
    if(isset($viewdata['tafsirdetail']->tafsir)){
      $pieces = explode(" ", $viewdata['tafsirdetail']->tafsir);
      $tafsirshort = implode(" ", array_splice($pieces, 0, 50));
    }else{
      $pieces = [];
      $tafsirshort = '';
    }
?>
    <meta property="og:title" content="Tafsir Surah {{$viewdata['surah_meta']->tname}} - {{$viewdata['surah_meta']->name}} Ayat ke {{$viewdata['ayah_range']}}">
    <meta property="og:description" content="<?php echo strip_tags($tafsirshort);?>">
    <meta property="og:url" content="https://www.quranku.id/tafsir/{{$viewdata['surah_meta']->slug}}/{{$viewdata['selectedAyah']}}">
    <meta property="og:image" content="https://www.quranku.id/assets/img/logo-text.jpg">
    <meta property="og:image:width" content="203" />
    <meta property="og:image:height" content="147" />
    <meta property="og:site_name" content="Quranku.id"/>
    
    
<?php } ?>
<?php 
$audiofile = str_pad($viewdata['ayahdetail']->SuraID,3,"0",STR_PAD_LEFT).str_pad($viewdata['ayahdetail']->VerseID,3,"0",STR_PAD_LEFT);
?>

@endsection

    @section('maincontent')
    
      <section class="jumbotron ">
        <div class="container">
         
        <div class="row ayah-row-detail">
        <div class="col-md-12">
        
        <h3>Tafsir Surat {{$viewdata['surah_meta']->tname}} - {{$viewdata['surah_meta']->name}} Ayat ke {{$viewdata['ayah_range']}}</h3>
        </div>
        <div class="col-md-12 ayah-tools-detail">

             @isset($viewdata['tafsirdetail']->asbab)<a href="#re"      onclick="gotoTarget('#asbabunuzul')">Asbabun Nuzul</a>@endisset
             @isset($viewdata['tafsirdetail']->tafsir)<a href="#re"     onclick="gotoTarget('#tafsir')">Tafsir</a>@endisset
             @isset($viewdata['tafsirdetail']->fiqih)<a href="#re"      onclick="gotoTarget('#fiqih')">Fiqih Kehidupan</a>@endisset
             @isset($viewdata['tafsirdetail']->kajian)<a href="#re"     onclick="gotoTarget('#kajian')">Kajian</a>@endisset
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#re" data-toggle="tooltip" onclick='playsound("{{$audiofile}}")' title="Bacakan" data-placement="bottom"><i id="audio-{{$audiofile}}" class="audiocontroller fas fa-play"></i></a>
            <a href="#re" class="st-custom-button" data-toggle="tooltip" data-url="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$viewdata['ayahdetail']->VerseID}}" data-network="facebook" title="Bagikan FB" data-placement="bottom"><i class="fab fa-facebook-f"></i></a>
            <a href="#re" class="st-custom-button" data-toggle="tooltip" data-url="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$viewdata['ayahdetail']->VerseID}}" data-network="twitter" title="Bagikan Twitter" data-placement="bottom"><i class="fab fa-twitter"></i></a>
            <a href="#re" class="st-custom-button" data-toggle="tooltip" data-url="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$viewdata['ayahdetail']->VerseID}}" data-network="whatsapp" title="Bagikan WhatsApp" data-placement="bottom"><i class="fab fa-whatsapp"></i></a>
            
        </div>
        </div>
            <div class="row ayah-row-detail">
                    <div class="col-md-6 col-sm-12 col-sm-push-6 translation-text">
                    <p>{{$viewdata['ayahdetail']->translation}}<p>
                   </div>
                   <div class="col-md-6 col-sm-12 col-sm-pull-6 arabic-text">
                     <?php
                       $ar = preg_replace("/بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ/","",$viewdata['ayahdetail']->arabic);
                       echo $ar."<br>";
                       
                     ?>
                   </div>
                   

            </div>
        
          
        </div>
      </section>

        <div class="container">
          @isset($viewdata['tafsirdetail']->asbab)
            @if($viewdata['tafsirdetail']->asbab != "")
              <div class="row" id="asbabunuzul">
                      <div class="col-md-12">
                              <h2>Asbabun Nuzul</h2>
                              {!!$viewdata['tafsirdetail']->asbab!!}
                      </div>
              </div>
            @endif
          @endisset
          
          @isset($viewdata['tafsirdetail']->tafsir)
            @if($viewdata['tafsirdetail']->tafsir != "")
                <div class="row"  id="tafsir">
                      <div class="col-md-12">
                              <h2>Tafsir</h2>
                              {!!$viewdata['tafsirdetail']->tafsir!!}
                      </div>
                </div>
            @endif
          @endisset

          @isset($viewdata['tafsirdetail']->fiqih)
          @if($viewdata['tafsirdetail']->fiqih != "")
              <div class="row"  id="fiqih">
                    <div class="col-md-12">
                            <h2>Fiqih Kehidupan</h2>
                            {!!$viewdata['tafsirdetail']->fiqih!!}
                    </div>
              </div>
          @endif
          @endisset


      </div>
   
	  @endsection


    @section('footer-js')

  

    <script>
       
    


        var playerstate = "stopped";
        var audioplayer = new Audio;


    function gotoTarget(target){
          var targetobj = $(target);
          var targetOffset = (targetobj.offset().top);
          $('html, body').animate({scrollTop: targetOffset}, 1000);
    }
      
      function playsound(soundfile){
          //get state
          if(playerstate=='stopped'){
            audioplayer.src = "/assets/sounds/"+soundfile+".mp3";
            audioplayer.loop = true;
            audioplayer.play();
            playerstate = "playing";
            $("#audio-"+soundfile).removeClass('fas fa-play');
            $("#audio-"+soundfile).addClass('fas fa-pause');
          }else {
            var sf = window.location.protocol+"//"+ window.location.host+"/assets/sounds/"+soundfile+".mp3";
            if(sf == audioplayer.src){
              if(playerstate=='playing'){
                //pause
                audioplayer.pause();
                playerstate = "paused";
                $(".audiocontroller").removeClass('fas fa-pause');
                $(".audiocontroller").addClass('fas fa-play');
              }else{
                //resume
                 audioplayer.play();
                 playerstate = "playing";
                 $("#audio-"+soundfile).removeClass('fas fa-play');
                $("#audio-"+soundfile).addClass('fas fa-pause');
              }

            }else{
              //play new ayah
              stopallsound();
              audioplayer.src = "/assets/sounds/"+soundfile+".mp3";
              audioplayer.loop = true;
              audioplayer.play();
              playerstate = "playing";
              $("#audio-"+soundfile).removeClass('fas fa-play');
              $("#audio-"+soundfile).addClass('fas fa-pause');
            }
          }
        }

        function stopallsound(){
          
          $(".audiocontroller").removeClass('fas fa-pause');
          $(".audiocontroller").addClass('fas fa-play');
        
          playerstate='stopped';
        }


    </script>

    @endsection
	  
   
