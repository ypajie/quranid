

@extends('layouts.app')
@section('title', 'Baca')

    @section('maincontent')
    
      <section class="jumbotron ">
        <div class="container">
         
         <div class="dropdown">
            <button class="btn dropdown-toggle btn-surah-selection" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Surah {{$viewdata['surah_meta']->tname}} - {{$viewdata['surah_meta']->name}}
            </button>
            <div class="dropdown-menu scrollable-menu" aria-labelledby="dropdownMenuButton">
                 @foreach ($viewdata['surahs'] as $surahs)
                  <a class="dropdown-item" href="/surah/{{$surahs->slug}}/">{{$surahs->tname}} - {{$surahs->name}} </a>
                @endforeach
            </div>
          </div>
           <audio id='audioplayer'>
              <source src="" type="audio/mpeg">
              Your browser does not support the audio element.
            </audio>
        </div>
      </section>

        <div class="container">
          <?php $audiofiles = '';?>
          @foreach ($viewdata['surah'] as $ayah)

          <div class="row ayah-row">
             
            <div class="col-md-6 col-sm-12 col-sm-push-6 translation-text">
             <p>{{$ayah->translation}}<p>
            </div>
            <div class="col-md-6 col-sm-12 col-sm-pull-6 arabic-text">
              <?php
                $ar = preg_replace("/بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ/","",$ayah->arabic);
                echo $ar."<br>";
                $audiofile = str_pad($ayah->SuraID,3,"0",STR_PAD_LEFT).str_pad($ayah->VerseID,3,"0",STR_PAD_LEFT);
                $audiofiles = $audiofiles . ",".$audiofile;
              ?>
            </div>
            <div class="col-md-12 ayah-tools">
              <a href="#re" data-toggle="tooltip" onclick='playsound("{{$audiofile}}")' title="Bacakan" data-placement="left"><i id="audio-{{$audiofile}}" class="audiocontroller fas fa-play"></i></a>
              <a href="#re" data-toggle="tooltip" title="Bagikan FB" data-placement="left"><i class="fab fa-facebook-f"></i></a>
              <a href="#re" data-toggle="tooltip" title="Bagikan Twitter" data-placement="left"><i class="fab fa-twitter"></i></a>
              <a href="#re" data-toggle="tooltip" title="Bagikan WhatsApp" data-placement="left"><i class="fab fa-whatsapp"></i></a>
              <a href="#re" data-toggle="tooltip" title="Asbabun Nuzul & Tafsir" data-placement="left"><i class="fas fa-book"></i></a>
             </div>
          </div>
          @endforeach

      </div>
   
	  @endsection


    @section('footer-js')

    <script>
      

      var strfiles = '{{$audiofiles}}';
        var playlist = ['','','','','','','',''];
        var playerstate = "stopped";
        var audioplayer = new Audio;
      
      function playsound(soundfile){
          //get state
          if(playerstate=='stopped'){
            audioplayer.src = "/assets/sounds/"+soundfile+".mp3";
            audioplayer.loop = true;
            audioplayer.play();
            playerstate = "playing";
            $("#audio-"+soundfile).removeClass('fas fa-play');
            $("#audio-"+soundfile).addClass('fas fa-pause');
          }else {
            var sf = window.location.protocol+"//"+ window.location.host+"/assets/sounds/"+soundfile+".mp3";
            if(sf == audioplayer.src){
              if(playerstate=='playing'){
                //pause
                audioplayer.pause();
                playerstate = "paused";
                $(".audiocontroller").removeClass('fas fa-pause');
                $(".audiocontroller").addClass('fas fa-play');
              }else{
                //resume
                 audioplayer.play();
                 playerstate = "playing";
                 $("#audio-"+soundfile).removeClass('fas fa-play');
                $("#audio-"+soundfile).addClass('fas fa-pause');
              }

            }else{
              //play new ayah
              stopallsound();
              audioplayer.src = "/assets/sounds/"+soundfile+".mp3";
              audioplayer.loop = true;
              audioplayer.play();
              playerstate = "playing";
              $("#audio-"+soundfile).removeClass('fas fa-play');
              $("#audio-"+soundfile).addClass('fas fa-pause');
            }
          }
        }

        function stopallsound(){
          
          $(".audiocontroller").removeClass('fas fa-pause');
          $(".audiocontroller").addClass('fas fa-play');
        
          playerstate='stopped';
        }


    </script>

    @endsection
	  
   
