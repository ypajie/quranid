

@extends('layouts.app')
@section('title', "Surah")


@section('opengraph')
<?php if(isset($viewdata['selectedAyah'])){ ?>
<?php
    $ogaudiofile = str_pad($viewdata['surah_meta']->ID,3,"0",STR_PAD_LEFT).str_pad($viewdata['selectedAyah'],3,"0",STR_PAD_LEFT);
?>
    <meta property="og:title" content="Surah {{$viewdata['surah_meta']->tname}} - {{$viewdata['surah_meta']->name}} Ayat ke {{$viewdata['selectedAyah']}}">
    <meta property="og:description" content="{{$viewdata['surah'][$viewdata['selectedAyah']-1]->translation}}">
    <meta property="og:url" content="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$viewdata['selectedAyah']}}">
    <meta property="og:type" content="music.song">
    <meta property="og:image" content="https://www.quranku.id/assets/img/logo-text.jpg">
    <meta property="og:image:width" content="203" />
    <meta property="og:image:height" content="147" />
    <meta property="og:site_name" content="Quranku.id"/>
    <meta property="og:audio" content="https://www.quran.id/assets/sounds/<?php echo $ogaudiofile;?>.mp3"/>
    
<?php }else{?>
  <meta property="og:title" content="Quranku - Surah {{$viewdata['surah_meta']->tname}} - {{$viewdata['surah_meta']->name}}">
  <meta property="og:description" content="Mesin Pencari Al Quran Cepat. Hasil pencarian dilengkapi dengan terjemahan, tafsir, riwayat dan audio">
  <meta property="og:url" content="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}">
  <meta property="og:type" content="website">
  <meta property="og:image" content="https://www.quranku.id/assets/img/logo-text.jpg">
  <meta property="og:image:width" content="203" />
  <meta property="og:image:height" content="147" />

<?php } ?>

@endsection

    @section('maincontent')
    
      <section class="jumbotron ">
        <div class="container">
         
         <div class="dropdown">
            <button class="btn dropdown-toggle btn-surah-selection" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Surah {{$viewdata['surah_meta']->tname}} - {{$viewdata['surah_meta']->name}}
            </button>
            <div class="dropdown-menu scrollable-menu" aria-labelledby="dropdownMenuButton">
                 @foreach ($viewdata['surahs'] as $surahs)
                  <a class="dropdown-item" href="/surah/{{$surahs->slug}}/">{{$surahs->tname}} - {{$surahs->name}} </a>
                @endforeach
            </div>
          </div>
          <p class='keterangan-surah'>
            <span id="ename">{{$viewdata['surah_meta']->ename}}<br></span>
            <span>{{$viewdata['surah_meta']->type}}</span>
            <span>- {{$viewdata['surah_meta']->ayahs}} ayat</span>

          </p>
          
        </div>
      </section>

        <div class="container">
          <?php $audiofiles = '';?>
          @foreach ($viewdata['surah'] as $ayah)

        <div class="row ayah-row" id="ayah-index-{{$ayah->VerseID}}">
             
            <div class="col-md-6 col-sm-12 col-sm-push-6 translation-text">
             <p>{{$ayah->translation}}<p>
            </div>
            <div class="col-md-6 col-sm-12 col-sm-pull-6 arabic-text">
              <?php
                $ar = preg_replace("/بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ/","",$ayah->arabic);
                echo $ar."<br>";
                $audiofile = str_pad($ayah->SuraID,3,"0",STR_PAD_LEFT).str_pad($ayah->VerseID,3,"0",STR_PAD_LEFT);
                $audiofiles = $audiofiles . ",".$audiofile;
              ?>
            </div>
            <div class="col-md-12 ayah-tools">
              <span>{{$viewdata['surah_meta']->tname}} Ayat ke-{{$ayah->VerseID}}</span>
              <a href="#re" data-toggle="tooltip" onclick='playsound("{{$audiofile}}")' title="Bacakan" data-placement="left"><i id="audio-{{$audiofile}}" class="audiocontroller fas fa-play"></i></a>
              <a href="#re" class="st-custom-button" data-toggle="tooltip" data-url="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$ayah->VerseID}}" data-network="facebook" title="Bagikan FB" data-placement="left"><i class="fab fa-facebook-f"></i></a>
              <a href="#re" class="st-custom-button" data-toggle="tooltip" data-url="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$ayah->VerseID}}" data-network="twitter" title="Bagikan Twitter" data-placement="left"><i class="fab fa-twitter"></i></a>
              <a href="#re" class="st-custom-button" data-toggle="tooltip" data-url="https://www.quranku.id/surah/{{$viewdata['surah_meta']->slug}}/{{$ayah->VerseID}}" data-network="whatsapp" title="Bagikan WhatsApp" data-placement="left"><i class="fab fa-whatsapp"></i></a>
              <a href="/tafsir/{{$viewdata['surah_meta']->slug}}/{{$ayah->VerseID}}" target="_blank" data-toggle="tooltip" title="Asbabun Nuzul & Tafsir" data-placement="left"><i class="fas fa-book"></i></a>
             </div>
          </div>
          @endforeach

      </div>
   
	  @endsection


    @section('footer-js')

  

    <script>
       
        <?php if(isset($viewdata['selectedAyah'])){ ?>
          var selectedAyah = <?php echo $viewdata['selectedAyah'];?>;
          var target = $('#ayah-index-'+selectedAyah);

          var targetOffset = (target.offset().top);
          $('html, body').animate({scrollTop: targetOffset}, 1000);
            
        <?php }?>

      

        var strfiles = '{{$audiofiles}}';
        var playlist = ['','','','','','','',''];
        var playerstate = "stopped";
        var audioplayer = new Audio;
      
    
      
      function playsound(soundfile){
          //get state
          if(playerstate=='stopped'){
            audioplayer.src = "/assets/sounds/"+soundfile+".mp3";
            audioplayer.loop = true;
            audioplayer.play();
            playerstate = "playing";
            $("#audio-"+soundfile).removeClass('fas fa-play');
            $("#audio-"+soundfile).addClass('fas fa-pause');
          }else {
            var sf = window.location.protocol+"//"+ window.location.host+"/assets/sounds/"+soundfile+".mp3";
            if(sf == audioplayer.src){
              if(playerstate=='playing'){
                //pause
                audioplayer.pause();
                playerstate = "paused";
                $(".audiocontroller").removeClass('fas fa-pause');
                $(".audiocontroller").addClass('fas fa-play');
              }else{
                //resume
                 audioplayer.play();
                 playerstate = "playing";
                 $("#audio-"+soundfile).removeClass('fas fa-play');
                $("#audio-"+soundfile).addClass('fas fa-pause');
              }

            }else{
              //play new ayah
              stopallsound();
              audioplayer.src = "/assets/sounds/"+soundfile+".mp3";
              audioplayer.loop = true;
              audioplayer.play();
              playerstate = "playing";
              $("#audio-"+soundfile).removeClass('fas fa-play');
              $("#audio-"+soundfile).addClass('fas fa-pause');
            }
          }
        }

        function stopallsound(){
          
          $(".audiocontroller").removeClass('fas fa-pause');
          $(".audiocontroller").addClass('fas fa-play');
        
          playerstate='stopped';
        }


    </script>

    @endsection
	  
   
