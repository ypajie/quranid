
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
    <meta name="description" content="Mesin Pencari Al Quran Cepat. Hasil pencarian dilengkapi dengan terjemahan, tafsir, riwayat dan audio">
    <meta name="author" content="">

    @yield('opengraph')

  

    <!-- TODO add manifest here -->
    <link rel="manifest" href="/manifest.json">
    <!-- Add to home screen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Quranku">
    <link rel="apple-touch-icon" href="assets/img/icon-152x152.png">
    <meta name="msapplication-TileImage" content="images/icons/icon-144x144.png">
    <meta name="msapplication-TileColor" content="#2F3BA2">
    
    <link rel="icon" href="../../../../favicon.ico">

    <title>QuranKu Beta - @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Khand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/instantsearch.js@2.10.2/dist/instantsearch.min.css">
    
    <!-- Custom styles for this template -->
    <link href="/assets/css/quranku.css" rel="stylesheet">
  </head>
  
    <body>
    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">Tentang Quranku</h4>
              <p class="text-muted">Quranku menyajikan Alquran lengkap dengan terjemahan, audio, tafsir dan asbabun nuzul yang dikaitkan di setiap ayatnya. Dilengkapi pula dengan fitur pencarian yang tidak hanya mencari didalam text terjemahan, 
                tapi juga bisa mencari di dalam tafsir, asbabun nuzul dan topik yang dibahas di dalam setiap ayat.  <a class="text-white" href='/tentang'>Selengkapnya...</a></p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Kontak</h4>
              <ul class="list-unstyled">
                <li><a href="https://www.facebook.com/qurankuid/" class="text-white">Facebook</a></li>
                <li><a href="mailto:ypajie@gmail.com" class="text-white">Email</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark shadow-sm navbar-fixed-top">
        <div class="container d-flex justify-content-between">
          <a href="/" class="navbar-brand d-flex align-items-center">
            <img src='/assets/img/logo-text.svg' width='80'>
            <!--<strong class="logo-text">Quranku</strong>-->
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>
 
  <body>
  
     <main role="main">
       	@yield('maincontent')
       </main>

 
<footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/holder.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@2.10.2"></script>
    <script src="/js/search.js"></script>


    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    </script>

    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5cd0658883f36f00193c8844&product=custom-share-buttons"></script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127729274-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127729274-1');
</script>

  
    
    @yield('footer-js')

</body>
</html>