

@extends('layouts.app')
@section('title', "Surah")


@section('opengraph')

  <meta property="og:title" content="Quranku - Tentang">
  <meta property="og:description" content="Mesin Pencari Al Quran Cepat. Hasil pencarian dilengkapi dengan terjemahan, tafsir, riwayat dan audio">
  <meta property="og:url" content="https://www.quranku.id/tentang">
  <meta property="og:type" content="website">
  <meta property="og:image" content="https://www.quranku.id/assets/img/logo-text.jpg">
  <meta property="og:image:width" content="203" />
  <meta property="og:image:height" content="147" />



@endsection

    @section('maincontent')
    
      <section class="jumbotron ">
        <div class="container">
         
         
           <h1>Tentang Quranku</h1>
           
         
          
        </div>
      </section>

        <div class="container">
                <p>Assalamualaikum warahmatullah wabarakatuh</p>
                <p>Rasa syukur yang teramat sangat saya panjatkan ke hadirat Allah SWT yang memungkinkan pengembangan website <a href='https://www.quranku.id'>Quranku.id</a> 
                    dapat diselesaikan untuk mengenang almarhumah istri tercinta Yeni Yuliati. 
                    Dengan harapan setiap manfaat yang dapat dipetik dari <a href='https://www.quranku.id'>Quranku.id</a> dapat menjadi pahala jariyah untuk almarhumah.</p>
                <p><a href='https://www.quranku.id'>Quranku.id</a>  dilengkapi dengan terjemahan, audio, tafsir dan asbabun nuzul yang dikaitkan 
                    di setiap ayatnya. Dilengkapi pula dengan fitur pencarian yang tidak hanya mencari didalam text 
                    terjemahan, tapi juga bisa mencari di dalam tafsir, asbabun nuzul dan topik yang dibahas di dalam 
                    setiap ayat. Dengan harapan dapat memudahkan para pengguna dalam mencari bahasan dalam AlQuran atas
                    masalah-masalah yang kita jumpai di keseharian.</p>
                <p>Sumber text arabic, terjemahan dan rekaman audio diambil dari <a href='https://www.tanzil.net'>Tanzil.net</a> , sebuah website yang menyediakan <i>resources</i> Alquran tidak hanya 
                    untuk pengguna tetapi juga untuk pemgembang aplikasi, agar dapat mengolah dan menyajikan kandungan isi 
                    Alquran dengan penambahan fitur-fitur yang memudahkan untuk mempelajari Alquran.</p>
                <p>Semoga pengguna sekalian dapat memetik sebesar-besar manfaat dari <a href='https://www.quranku.id'>Quranku.id</a> </p>

                <p>Wassalamualaikum warahmatullah wabarakatuh</p>
                <p><img src='assets/img/ttd-ypa.png'></p>

        </div>
   
	  @endsection


    @section('footer-js')

  

    

    @endsection
	  
   
