

@extends('layouts.app')
@section('title', 'Beranda')

@section('opengraph')

    <meta property="og:title" content="Quranku">
    <meta property="og:description" content="Mesin Pencari Al Quran Cepat. Hasil pencarian dilengkapi dengan terjemahan, tafsir, riwayat dan audio">
    <meta property="og:url" content="https://www.quranku.id">
    <meta property="og:image" content="https://www.quranku.id/assets/img/logo-text.jpg">
    <meta property="og:image:width" content="203" />
    <meta property="og:image:height" content="147" />

@endsection

   
    
    @section('maincontent')
    
      <section class="jumbotron ">
        <div class="container">
            <input type="text" class="form-control searchinput" placeholder="Ketik kata kunci pencarian di sini" aria-label="Pencarian">
          <p style="padding:5px 5px 5px 5px;"><cite>Kata kunci pencarian bisa berupa nama surah, penggalan ayat, penggalan tafsir atau penggalan kata lainnya yang Anda ingat</cite></p>

        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container search-results">
          
          <script type="text/html" id="hit-template">
                <div class="row ayah-row">
    
                  <div class="col-md-6 col-md-push-6 translation-text">
                    <a href='surah/@{{surah_slug}}/@{{ayah_index}}'>
                      <h4>@{{surah_tname}} Ayat ke @{{ayah_index}}</h4>
                      <p>@{{{_highlightResult.bahasa.value}}}<p>
                    </a>
                  </div>
                  <div class="col-md-6 col-md-pull-6 arabic-text">
                    @{{arabic}}
                  </div>

                </div>

          </script>


        </div>


        <div class="container">
         

          <div class="row">

            @foreach ($viewdata['surahs'] as $surahs)

            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <div class='surah-name'><h1>{{$surahs->name}}</h1></div>
                <div class="card-body">
                  <p class="card-text">Surat {{$surahs->tname}}</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     <a href='/surah/{{$surahs->slug}}/' class="btn btn-sm btn-outline-secondary">Baca</a>
                      
                    </div>
                    <small class="text-muted">{{$surahs->ayahs}} Ayat</small>
                  </div>
                </div>
              </div>
            </div>
            @endforeach

            
          </div>
        </div>
      </div>
	  @endsection
	  
   
