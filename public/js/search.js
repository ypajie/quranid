const search = instantsearch({
	appId: 'IM3G5LPHPY',
	apiKey: '49f4ed7b388d9770506efb7b4e8ff3d2',
	indexName: 'quranku-v1',
	routing: true,

	searchFunction: function(helper) {
				var searchResults = $('.search-results');
				if (helper.state.query === '') {
					//reset search ketika query kosong
					return;
				}

				helper.search();
		    	searchResults.show();
		}
	});


search.addWidget(
	instantsearch.widgets.searchBox({
		container: '.searchinput',
		reset: false,
		magnifier: false,
	})
);



search.addWidget(
	instantsearch.widgets.hits({
		container:'.search-results',
		hitsPerPage:10,
		transformData: {
			item: function(data) {
					//potong kata tentang yang paling depan
				  
			    return data;
			    }
		},
		templates: {
        empty: 'No results',
        item: document.getElementById('hit-template').innerHTML
      }
		})
);

//search.addWidget( instantsearch.widgets.pagination({ container: '#hits-pagination' }) );
search.start();
